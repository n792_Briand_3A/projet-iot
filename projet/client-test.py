# ajouter la lib paho : pip install paho
# python2.7
import paho.mqtt.client as mqtt
import json
import base64
import logging
import os, re

# Fonction définition
def creation_fichier (nomFichier):
    newFile = "./" + nomFichier + ".kml"
    with open(newFile, 'a'):
        os.utime(newFile, None)
    poignee = open (newFile, 'w')
    return poignee

def add_line_fichier (line, fichier_dest):
    fichier_dest.write(line)

# config
mqttServer = "loraserver.tetaneutral.net"
appID = "29"
deviceID = "b5f43a65b1b4c220"

# du log pour debug
logging.basicConfig(level=logging.DEBUG)

#Création fichier output
output_file_name = input("Entrez le chemin et le nom du fichier de sortie : ")
output_file = creation_fichier(output_file_name)

# callback appele lors de la reception d un message
def on_message(mqttc, obj, msg):
    jsonMsg = json.loads(msg.payload)

    # Recuperation Valeur
    time = jsonMsg["rxInfo"][0]["time"]
    data = base64.b64decode(jsonMsg["data"])
    data_split = data.split("/")
    latitude = data_split[0]
    longitude = data_split[1]
    SF_tx = jsonMsg["txInfo"][0]["loRaModulationInfo"][0]["spreadingFactor"]
    rssi = jsonMsg["rxInfo"][0]["rssi"]
    snr = jsonMsg["rxInfo"][0]["loRaSNR"]
    fcnt = jsonMsg[""]

    line = time + " " + latitude + " " + longitude + " " + SF_tx + " " + snr + " " + rssi + " " + fcnt
    add_line_fichier(line)

# creation du client
mqttc = mqtt.Client()

mqttc.on_message = on_message

logger = logging.getLogger(__name__)

mqttc.enable_logger(logger)

mqttc.connect(mqttServer, 1883, 60)

# soucription au device
mqttc.subscribe("application/"+appID+"/device/"+deviceID+"/rx", 0)

mqttc.loop_forever()

