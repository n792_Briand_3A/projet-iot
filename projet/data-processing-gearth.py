import os, re

def lecture_fichier (nomFichier):
    emplacement_fichier = "./" + nomFichier
    fichier = open(emplacement_fichier,'r')
    return fichier

def creation_fichier (nomFichier):
    newFile = "./" + nomFichier + ".kml"
    with open(newFile, 'a'):
        os.utime(newFile, None)
    poignee = open (newFile, 'w')
    return poignee

def add_line_fichier (fichier_a_ajouter, fichier_dest):
    # fadd = open(fichier_a_ajouter, 'r')
    # fdest = open(fichier_dest, 'a+')
    fichier_dest.write(fichier_a_ajouter.read())

def calcul_coordonnees (degre, minute, seconde, direction):
    dd = float(int(degre)) + float(int(minute))/60.0 + float(int(seconde))/(3600.0)
    if direction == 'S' or direction == 'O':
        dd *= -1
    return dd

def color_placemark (down_rssi):
    if down_rssi == "-------":
        return "msn_wht-circle"
    else:
        # Recupération du RSSI sans le dBm
        rssi_string = down_rssi[:-3]
        #Transformation en valeur entiere
        rssi = int(rssi_string)

        if rssi>=-80:
            return "msn_red-circle"
        elif rssi>=-120:
            return "msn_grn-circle"
        elif rssi>=-140:
            return "msn_blu-circle"
        else:
            return "msn_wht-circle"

def creation_placemark (Fichier, Color, Time, Latitude, Longitude, UP_SF, UP_Frequency, UP_Power, UP_SNR, UP_Q, DOWN_SF, DOWN_Frequency, DOWN_RSSI, DOWN_SNR, DOWN_Q, CNT_UL, CNT_DL, CNT_PER):
    #ecrire_fichier = open (Fichier, 'a')
    Fichier.write("<Placemark>")
    Fichier.write("  <name>"+Time+"</name>")
    Fichier.write("  <Snippet maxLines=\"0\"></Snippet>")
    Fichier.write("  <description><![CDATA[<style type='text/css'>*{font-family:Verdana,Arial,Helvetica,Sans-Serif;box-sizing:border-box;padding:0px;margin:0px}table{border-collapse:collapse;border-spacing:0px;margin:2px}.trb{background-color:#ddffdd}.ch{vertical-align:top;padding-left:10px;white-space:nowrap}.cv{vertical-align:top;padding-left:6px;padding-right:10px;white-space:nowrap}.cw{vertical-align:top;padding-left:6px;padding-right:10px;width:600px}</style><table><tr class='trb'><td class='ch'><b>Heure</b></td><td class='cv'>"+Time+"</td></tr><tr><td class='ch'><b>Latitude</b></td><td class='cv'>"+Latitude+"</td></tr><tr class='trb'><td class='ch'><b>Longitude</b></td><td class='cv'>"+Longitude+"</td></tr><tr><td class='ch'><b>UP SF</b></td><td class='cv'>"+UP_SF+"</td></tr><tr class='trb'><td class='ch'><b>UP Frequence</b></td><td class='cv'>"+UP_Frequency+"</td></tr><tr><td class='ch'><b>UP Puissance</b></td><td class='cv'>"+UP_Power+"</td></tr><tr class='trb'><td class='ch'><b>UP SNR</b></td><td class='cv'>"+UP_SNR+"</td></tr><tr><td class='ch'><b>UP Q</b></td><td class='cv'>"+UP_Q+"</td></tr><tr class='trb'><td class='ch'><b>DOWN SF</b></td><td class='cv'>"+DOWN_SF+"</td></tr><tr><td class='ch'><b>DOWN Fréquence</b></td><td class='cv'>"+DOWN_Frequency+"</td></tr><tr class='trb'><td class='ch'><b>DOWN RSSI</b></td><td class='cv'>"+DOWN_RSSI+"</td></tr><tr><td class='ch'><b>DOWN SNR</b></td><td class='cv'>"+DOWN_SNR+"</td></tr><tr class='trb'><td class='ch'><b>DOWN Q</b></td><td class='cv'>"+DOWN_Q+"</td></tr><tr><td class='ch'><b>CNT UL</b></td><td class='cv'>"+CNT_UL+"</td></tr><tr class='trb'><td class='ch'><b>CNT DL</b></td><td class='cv'>"+CNT_DL+"</td></tr><tr><td class='ch'><b>CNT PER</b></td><td class='cv'>"+CNT_PER+"</td></tr></table>]]></description>")
    Fichier.write("  <LookAt>")
    Fichier.write("      <longitude>"+Longitude+"</longitude>")
    Fichier.write("      <latitude>"+Latitude+"</latitude>")
    Fichier.write("      <altitude>15</altitude>")
    Fichier.write("      <heading>0</heading>")
    Fichier.write("      <tilt>0</tilt>")
    Fichier.write("      <range>10000</range>")
    Fichier.write("      <altitudeMode>relativeToGround</altitudeMode>")
    Fichier.write("  </LookAt>")
    Fichier.write("  <styleUrl>#"+Color+"</styleUrl>")
    Fichier.write("  <Point>")
    Fichier.write("      <extrude>1</extrude>")
    Fichier.write("      <altitudeMode>relativeToGround</altitudeMode>")
    Fichier.write("      <coordinates>"+Longitude+","+Latitude+",15</coordinates>")
    Fichier.write("  </Point>")
    Fichier.write("</Placemark>")

def main():
    #Introduction
    print("########################################################################################")
    print("# Bienvenue dans le programme de traitement des mesures LoRaWan obtenues avec l'ESP-32 #")
    print("########################################################################################")

    #Emplacement des fichiers
    input_file_name = "./Entry/Input.txt"
    header_path = "./Assets/Header.kml"
    style_path = "./Assets/Style.kml"
    footer_path = "./Assets/Footer.kml"

    #Lecture fichier
    input_file = lecture_fichier(input_file_name)
    

    #Création du fichier de sortie
    output_file_name = input("Entrez le chemin et le nom du fichier de sortie : ")
    output_file = creation_fichier(output_file_name)

    #Ajout du Header au fichier d'output
    add_line_fichier(lecture_fichier(header_path), output_file)

    #Ajout du nom du fichier et de la description
    output_file.write("<name>$NomSite</name>")
    output_file.write("<description><![CDATA[<style type='text/css'>*{font-family:Verdana,Arial,Helvetica,Sans-Serif;}</style><table style=\"width: 300px;\"><tr><td style=\"vertical-align: top;\">Source</td><td style=\"width: 100%%;\">Input.txt</td></tr><tr><td>Date</td><td>$Date</td><tr><td style=\"vertical-align: top;\">Développeur</td><td style=\"width: 100%%;\">Paul BOUSSARD - Expertise / AEI<br/></td></tr></tr><tr><td colspan=\"2\" style=\"vertical-align: top;\"><br/>Programme créer dans le but d'exploiter les mesures de couverture LoRaWan obtenu à l'aide du Fiel Test Device d'Adeunis et récupéré à partir des logs de l'appareil dans le logiciel IoTConfiguratorApp.</td></tr></table>]]></description>")

    #Ajout du style au fichier
    add_line_fichier(lecture_fichier(style_path), output_file)

    #Ajout des Placemark au Fichier destination
    #Lecture des lignes du fichier source de donnée
    for line in input_file:
        #Split de l'ensemble des mot de la ligne dans un tableau
        words_line_with_spaces = line.split(' ')

        #Supprimer
        words_line = []
        for caractere in words_line_with_spaces:
            if(caractere != ''):
                words_line.append(caractere)

        #Si des coordonnées GPS sont présent, on traite la ligne sinon on fait rien
        #Description du pattern à match
        pattern = re.compile("\d\d:\d\d:\d\d")
        
        if ((pattern.match(words_line[0])) and (words_line[0] != "--:--:--")):
            # Détermination des coordonnées GPS au format degré décimal
            latitude = str(calcul_coordonnees(words_line[1], words_line[2], words_line[3], words_line[4]))
            longitude = str(calcul_coordonnees(words_line[5], words_line[6], words_line[7], words_line[8]))

            # Determination de la couleur du Placemark 
            color = color_placemark(words_line[16])

            # Ajout du placemark au fichier destination
            creation_placemark(output_file, color, words_line[0], latitude, longitude, words_line[9], words_line[10], words_line[11], words_line[12], words_line[13], words_line[14], words_line[15], words_line[16], words_line[17], words_line[18], words_line[19], words_line[20], words_line[21])

    add_line_fichier(lecture_fichier(footer_path), output_file)


main()
